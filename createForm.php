<!DOCTYPE html>
<html>
<head>
<title>MGF Exercise</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
</head>
<body>
    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">Create a Design</a>
        </div>
    </div>
    <div id="main" class="container">
      <form id="createdesign" name="designForm" method="POST" action="">
          Customer Id: <input type="text" name="customer_id">
          <br>
          Location Id: <input type="text" name="location_id">
            <br>
          Contractor Id: <input type="text" name="contractor_id">
          <br>
          Title: <input type="text" name="title">
          <br>
          Special Project: <input type="text" name="special_project">
          <br>
          Permanent Works: <input type="text" name="permanent_works">
          <br>
          Depth: <input type="text" name="depth">
          <br>
          Length: <input type="text" name="length">
          <br>
          Width: <input type="text" name="width">
          <br>
          Design Type Id: <input type="text" name="design_type_id">
          <br>
          <input type="submit" value="Submit">
      </form>
    </div>

<script>

function toJSONString( form ) {
  var obj = {};
  var elements = form.querySelectorAll( "input, select, textarea" );
  for( var i = 0; i < elements.length; ++i ) {
    var element = elements[i];
    var name = element.name;
    var value = element.value;

    if( name ) {
      obj[ name ] = value;
    }
  }

  return JSON.stringify( obj );
}

function createdesign(json) {

  // Create a request variable and assign a new XMLHttpRequest object to it.
    var delrequest = new XMLHttpRequest()
    delrequest.open('POST', 'http://192.168.0.33/webserver/MGF%20exercise/api/class/design/create.php', true)

    delrequest.send(json);

    delrequest.onreadystatechange = function(){
      if (this.readyState == 4 && this.status == 201) {
        alert('created')
        reload()
      } else if(this.readyState == 4 && this.status != 201) {
        alert('An error occured whilst creating the design')
      }
    }
  }


  // Event listener for the submit button 
	document.addEventListener( "DOMContentLoaded", function() {
		var form = document.getElementById( "createdesign" );
		//var output = document.getElementById( "output" );
		form.addEventListener( "submit", function( e ) {
			e.preventDefault();
			var json = toJSONString( this );
			createdesign(json);

		}, false);


})


</script>


</body>
</html>
