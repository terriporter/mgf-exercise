<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../../../conn/database.php';
include_once 'design_categories.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare design category item
$design_category = new Design_Category($db);

// set ID property of record to read
$design_category->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of design category to be edited
$design_category->readOne();

if($design_category->id!=null){
	// create array
	$design_category_arr = array(
		"id" =>  $design_category->id,
    "description" => $design_category->description,
		"turnaround_time" => $design_category->turnaround_time,
		"hex_color" => $design_category->hex_color,
    "rgb_color" => $design_category->rgb_color,
    "created_at" => $design_category->created_at,
    "created_by_id" => $design_category->created_by_id,
    "updated_at" => $design_category->updated_at,
    "updated_by_id" => $design_category->updated_by_id,
    "deleted_by_id" => $design_category->deleted_by_id,
    "deleted_at" => $design_category->deleted_at

	);

	// set response code - 200 OK
	http_response_code(200);

	// make it json format
	echo json_encode($design_category_arr);
}

else{
	// set response code - 404 Not found
    http_response_code(404);

	// tell the user the design category does not exist
	echo json_encode(array("message" => "Design Category does not exist."));
}
?>
