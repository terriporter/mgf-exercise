<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../../conn/database.php';
include_once 'design_categories.php';

// instantiate database
$database = new Database();
$db = $database->getConnection();

// initialize object
$design_category= new Design_Category($db);

// query design category
$stmt = $design_category->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){

	// design category array
	$design_category_arr=array();
	$design_category_arr["records"]=array();

	// retrieve our table contents
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		// extract row
		// this will make $row['name'] to
		// just $name only
		extract($row);
    //print_r($row);
		$design_category_item=array(
			"id" => $id,
      "description" => $description,
			"turnaround_time" => $turnaround_time,
      "hex_color" => $hex_color,
      "rgb_color" => $rgd_color,
      "created_at" => $created_at,
      "created_by_id" => $created_by_id,
      "updated_at" => $updated_at,
      "updated_by_id" => $updated_by_id,
      "deleted_by_id" => $deleted_by_id,
			"deleted_at" => $deleted_at
		);

		array_push($design_category_arr["records"], $design_category_item);
	}

	// set response code - 200 OK
	http_response_code(200);

	// show design category data in json format
	echo json_encode($design_category_arr);
}

// no design categories found will be here

else{

	// set response code - 404 Not found
	http_response_code(404);

	// tell the user no design categories found
	echo json_encode(
		array("message" => "No Design Categories found.")
	);
}




?>
