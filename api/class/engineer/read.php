<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../../conn/database.php';
include_once 'engineer.php';

// instantiate database
$database = new Database();
$db = $database->getConnection();

// initialize object
$engineer= new Engineer($db);

// query engineer
$stmt = $engineer->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){

	// engineer array
	$engineers_arr=array();
	$engineers_arr["records"]=array();

	// retrieve our table contents
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		// extract row
		// this will make $row['name'] to
		// just $name only
		extract($row);
    //print_r($row);
		$engineer_item=array(
			"id" => $id,
      "userid" => $userid,
			"name" => $name,
      "initial" => $initial,
      "grade" => $grade,
      "title" => $title,
      "qualifications" => $qualifications,
      "eductaion" => $eductaion
		);

		array_push($engineers_arr["records"], $engineer_item);
	}

	// set response code - 200 OK
	http_response_code(200);

	// show products data in json format
	echo json_encode($engineers_arr);
}

// no engineers found will be here

else{

	// set response code - 404 Not found
	http_response_code(404);

	// tell the user no engineers found
	echo json_encode(
		array("message" => "No Engineers found.")
	);
}




?>
