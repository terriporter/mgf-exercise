<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../../../conn/database.php';
include_once 'engineer.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare engineer object
$engineer = new Engineer($db);

// set ID property of record to read
$engineer->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of engineer to be edited
$engineer->readOne();

if($engineer->userid!=null){
	// create array
	$engineer_arr = array(
		"id" =>  $engineer->id,
    "userid" => $engineer->userid,
		"name" => $engineer->name,
		"initial" => $engineer->initial,
    "grade" => $engineer->grade,
    "title" => $engineer->title,
    "qualifications" => $engineer->qualifications,
    "eductaion" => $engineer->eductaion

	);

	// set response code - 200 OK
	http_response_code(200);

	// make it json format
	echo json_encode($engineer_arr);
}

else{
	// set response code - 404 Not found
    http_response_code(404);

	// tell the user engineer does not exist
	echo json_encode(array("message" => "Engineer does not exist."));
}
?>
