<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object file
include_once '../../../conn/database.php';
include_once 'design_issues.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare design issue object
$design_issues = new Design_Issues($db);

// get design issue id
$data = json_decode(file_get_contents("php://input"));

// set design issue id to be deleted
$design_issues->id = $data->id;

// delete the design issue
if($design_issues->delete()){

	// set response code - 200 ok
	http_response_code(200);

	// tell the user
	echo json_encode(array("message" => "Design Issue was deleted."));
}

// if unable to delete the design issue
else{

	// set response code - 503 service unavailable
	http_response_code(503);

	// tell the user
	echo json_encode(array("message" => "Unable to delete design issue."));
}
?>
