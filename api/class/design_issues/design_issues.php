<?php
class Design_Issues{

	// database connection and table name
	private $conn;
	private $table_name = "design_issues";

	// object properties
	public $id;
	public $design_id;
	public $category_id;
	public $description;
	public $date_in;
	public $date_out;
	public $designer_id;
  public $checker_id;
  public $status_id;
  public $drawing_req;

	// constructor with $db as database connection
	public function __construct($db){
		$this->conn = $db;
	}


// read design issues
function read(){

	// select all query
	$query = "SELECT
			*
			FROM
				" . $this->table_name;

	// prepare query statement
	$stmt = $this->conn->prepare($query);

	// execute query
	$stmt->execute();

	return $stmt;
}

//retreiving the data through the design id
function getByDesign(){

	// select all query
	$query = "SELECT * FROM " . $this->table_name. " WHERE design_id = ?";
error_log($query);
	// prepare query statement
	$stmt = $this->conn->prepare($query);
	error_log('The design id is '.$this->design_id);
	$stmt->bindParam(1, $this->design_id);
	// execute query
	$stmt->execute();

	return $stmt;
}


function readOne(){

	// query to read single record
	$query = "SELECT
				*
			FROM
				" . $this->table_name . "
			WHERE
				id = ?
			LIMIT
				0,1";

	// prepare query statement
	$stmt = $this->conn->prepare( $query );

	// bind id of design issue to be updated
	$stmt->bindParam(1, $this->id);

	// execute query
	$stmt->execute();

	// get retrieved row
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	// set values to object properties
  $this->id = $row['id'];
  $this->design_id = $row['design_id'];
  $this->category_id = $row['category_id'];
  $this->description = $row['description'];
  $this->date_in = $row['date_in'];
  $this->date_out = $row['date_out'];
  $this->designer_id = $row['designer_id'];
  $this->checker_id = $row['checker_id'];
  $this->status_id = $row['status_id'];
  $this->drawing_req = $row['drawing_req'];
}

// update the design issue
function update(){

	//Check if the designer and checker are the same user
	if ($this->checker_id == $this->designer_id) {
		return false;
	} else {

	// update query
	$query = "UPDATE
				" . $this->table_name . "
			SET
				design_id = :design_id,
				category_id = :category_id,
				description = :description,
				date_in = :date_in,
        date_out = :date_out,
        designer_id = :designer_id,
        checker_id = :checker_id,
        status_id = :status_id,
        drawing_req = :drawing_req,
    WHERE
				id = :id";

	// prepare query statement
	$stmt = $this->conn->prepare($query);

	// sanitize
	$this->design_id=htmlspecialchars(strip_tags($this->design_id));
	$this->category_id=htmlspecialchars(strip_tags($this->category_id));
	$this->description=htmlspecialchars(strip_tags($this->description));
	$this->designer_id=htmlspecialchars(strip_tags($this->designer_id));
  $this->checker_id=htmlspecialchars(strip_tags($this->checker_id));
  $this->status_id=htmlspecialchars(strip_tags($this->status_id));
  $this->drawing_req=htmlspecialchars(strip_tags($this->drawing_req));

	// bind new values
  $stmt->bindParam(':id', $this->id);
  $stmt->bindParam(':design_id', $this->design_id);
	$stmt->bindParam(':category_id', $this->category_id);
	$stmt->bindParam(':description', $this->description);
	$stmt->bindParam(':date_in', $this->date_in);
  $stmt->bindParam(':date_out', $this->date_out);
  $stmt->bindParam(':designer_id', $this->designer_id);
  $stmt->bindParam(':checker_id', $this->checker_id);
  $stmt->bindParam(':status_id', $this->status_id);
  $stmt->bindParam(':drawing_req', $this->drawing_req);

	// execute the query
	if($stmt->execute()){
		return true;
	}

	return false;
}
}

// delete the design issue
function delete(){

	// delete query
	$query = "DELETE FROM " . $this->table_name . " WHERE id = ?";

	// prepare query
	$stmt = $this->conn->prepare($query);

	// sanitize
	$this->id=htmlspecialchars(strip_tags($this->id));

	// bind id of record to delete
	$stmt->bindParam(1, $this->id);

	// execute query
	if($stmt->execute()){
		return true;
	}

	return false;

}

// create design issue
function create(){

	//Check if the designer and checker are the same user
	if ($this->checker_id == $this->designer_id) {
		return false;
	} else {

	// query to insert record
	$query = "INSERT INTO
				" . $this->table_name . "
			SET
				design_id=:design_id, category_id=:category_id, description=:description, date_in=:date_in, date_out=:date_out, designer_id=:designer_id, checker_id=:checker_id, status_id=:status_id, drawing_req=:drawing_req";

	// prepare query
	$stmt = $this->conn->prepare($query);

  // sanitize
  $this->design_id=htmlspecialchars(strip_tags($this->design_id));
  $this->category_id=htmlspecialchars(strip_tags($this->category_id));
  $this->description=htmlspecialchars(strip_tags($this->description));
  $this->designer_id=htmlspecialchars(strip_tags($this->designer_id));
  $this->checker_id=htmlspecialchars(strip_tags($this->checker_id));
  $this->status_id=htmlspecialchars(strip_tags($this->status_id));
  $this->drawing_req=htmlspecialchars(strip_tags($this->drawing_req));

  // bind new values
  $stmt->bindParam(':id', $this->id);
  $stmt->bindParam(':design_id', $this->design_id);
  $stmt->bindParam(':category_id', $this->category_id);
  $stmt->bindParam(':description', $this->description);
  $stmt->bindParam(':date_in', $this->date_in);
  $stmt->bindParam(':date_out', $this->date_out);
  $stmt->bindParam(':designer_id', $this->designer_id);
  $stmt->bindParam(':checker_id', $this->checker_id);
  $stmt->bindParam(':status_id', $this->status_id);
  $stmt->bindParam(':drawing_req', $this->drawing_req);

	// execute query
	if($stmt->execute()){
		return true;
	}

	return false;

}
}
}

?>
