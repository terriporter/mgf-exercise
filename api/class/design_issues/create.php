<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../../conn/database.php';

// instantiate design issue object
include_once 'design_issues.php';

$database = new Database();
$db = $database->getConnection();

$design_issues = new Design_Issues($db);

// get posted data
$data = json_decode(file_get_contents("php://input"));

// make sure data is not empty
if(
	!empty($data->design_id) &&
	!empty($data->category_id) &&
	!empty($data->description) &&
	!empty($data->date_in) &&
  !empty($data->date_out) &&
  !empty($data->designer_id) &&
  !empty($data->checker_id) &&
  !empty($data->status_id) &&
  !empty($data->drawing_req)
){

  // set design property values
  $design_issues->design_id = $data->design_id;
  $design_issues->category_id = $data->category_id;
  $design_issues->description = $data->description;
  $design_issues->date_in = $data->date_in;
  $design_issues->date_out = $data->date_out;
  $design_issues->designer_id = $data->designer_id;
  $design_issues->checker_id = $data->checker_id;
  $design_issues->status_id = $data->status_id;
  $design_issues->drawing_req = $data->drawing_req;

	if($design_issues->create()){

		// set response code - 201 created
		http_response_code(201);

		// tell the user
		echo json_encode(array("message" => "Design Issues was created."));
	}

	// if unable to create the design issue, tell the user
	else{

		// set response code - 503 service unavailable
	    http_response_code(503);

		// tell the user
		echo json_encode(array("message" => "Unable to create Design Issue."));
	}
}

// tell the user data is incomplete
else{

	// set response code - 400 bad request
	http_response_code(400);

	// tell the user
	echo json_encode(array("message" => "Unable to create Design Issue. Data is incomplete."));
}
?>
