<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../../conn/database.php';
include_once 'design_issues.php';

// instantiate database
$database = new Database();
$db = $database->getConnection();

// initialize object
$design_issues = new Design_Issues($db);

// query design issue
$stmt = $design_issues->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){

	// design array
	$design_issues_arr=array();
	$design_issues_arr["records"]=array();

	// retrieve our table contents
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		// extract row
		// this will make $row['name'] to
		// just $name only
		extract($row);
    //print_r($row);
		$design_issue_item=array(
			"id" => $id,
      "design_id" => $design_id,
			"category_id" => $category_id,
      "description" => $description,
      "date_in" => $date_in,
      "date_out" => $date_out,
      "designer_id" => $designer_id,
      "checker_id" => $checker_id,
      "status_id" => $status_id,
      "drawing_req" => $drawing_req
		);

		array_push($design_issues_arr["records"], $design_issue_item);
	}

	// set response code - 200 OK
	http_response_code(200);

	// show design issues data in json format
	echo json_encode($design_issues_arr);
}

// no design issues found will be here

else{

	// set response code - 404 Not found
	http_response_code(404);

	// tell the user no designs found
	echo json_encode(
		array("message" => "No Design Issues found.")
	);
}




?>
