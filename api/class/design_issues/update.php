<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../../../conn/database.php';
include_once 'design_issues.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare design issue object
$design_issues = new Design_Issues($db);

// get id of design issue to be edited
$data = json_decode(file_get_contents("php://input"));

// set ID property of product to be edited
$design_issues->id = $data->id;

// set design property values
$design_issues->design_id = $data->design_id;
$design_issues->category_id = $data->category_id;
$design_issues->description = $data->description;
$design_issues->date_in = $data->date_in;
$design_issues->date_out = $data->date_out;
$design_issues->designer_id = $data->designer_id;
$design_issues->checker_id = $data->checker_id;
$design_issues->status_id = $data->status_id;
$design_issues->drawing_req = $data->drawing_req;

// update the design issue
if($design_issues->update()){

	// set response code - 200 ok
	http_response_code(200);

	// tell the user
	echo json_encode(array("message" => "Design Issue was updated."));
}

// if unable to update the design issue, tell the user
else{

	// set response code - 503 service unavailable
	http_response_code(503);

	// tell the user
	echo json_encode(array("message" => "Unable to update design issue."));
}
?>
