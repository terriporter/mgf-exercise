<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../../../conn/database.php';
include_once 'design_issues.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare design issue object
$design_issues = new Designs($db);

// set ID property of record to read
$design_issues->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of design issue to be edited
$design_issues->readOne();

if($design_issues->id!=null){
	// create array
	$design_issues_arr = array(
		"id" =>  $design_issues->id,
    "design_id" => $design_issues->design_id,
		"category_id" => $design_issues->category_id,
		"description" => $design_issues->description,
    "date_in" => $design_issues->date_in,
    "date_out" => $design_issues->date_out,
    "designer_id" => $design_issues->designer_id,
    "checker_id" => $design_issues->checker_id,
    "status_id" => $design_issues->status_id,
    "drawing_req" => $design_issues->drawing_req

	);

	// set response code - 200 OK
	http_response_code(200);

	// make it json format
	echo json_encode($design_issues_arr);
}

else{
	// set response code - 404 Not found
    http_response_code(404);

	// tell the user design issue does not exist
	echo json_encode(array("message" => "Design Issues does not exist."));
}
?>
