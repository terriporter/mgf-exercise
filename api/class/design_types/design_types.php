<?php
class Design_Types{

	// database connection and table name
	private $conn;
	private $table_name = "design_types";

	// object properties
	public $id;
	public $description;
	public $icon;
	public $created_at;
	public $created_by_id;
  public $modified_timestamp;
  public $modified_by_id;
  public $deleted_by_id;
  public $deleted_at;

	// constructor with $db as database connection
	public function __construct($db){
		$this->conn = $db;
	}


// read design types
function read(){

	// select all query
	$query = "SELECT
			*
			FROM
				" . $this->table_name;

	// prepare query statement
	$stmt = $this->conn->prepare($query);

	// execute query
	$stmt->execute();

	return $stmt;
}

function readOne(){

	// query to read single record
	$query = "SELECT
				*
			FROM
				" . $this->table_name . "
			WHERE
				id = ?
			LIMIT
				0,1";

	// prepare query statement
	$stmt = $this->conn->prepare( $query );

	// bind id of product to be updated
	$stmt->bindParam(1, $this->id);

	// execute query
	$stmt->execute();

	// get retrieved row
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	// set values to object properties
  $this->description = $row['description'];
  $this->icon = $row['icon'];
	$this->created_at = $row['created_at'];
	$this->created_by_id = $row['created_by_id'];
  $this->modified_timestamp = $row['modified_timestamp'];
  $this->updated_by_id = $row['modified_by_id'];
  $this->deleted_by_id = $row['deleted_by_id'];
  $this->deleted_at = $row['deleted_at'];
}






}

?>
