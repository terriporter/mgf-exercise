<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../../../conn/database.php';
include_once 'design_types.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare design type item
$design_types = new Design_Types($db);

// set ID property of record to read
$design_types->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of design types to be edited
$design_types->readOne();

if($design_types->id!=null){
	// create array
	$design_types_arr = array(
		"id" =>  $design_types->id,
    "description" => $design_types->description,
		"icon" => $design_types->icon,
    "created_at" => $design_types->created_at,
    "created_by_id" => $design_types->created_by_id,
    "modified_timestamp" => $design_types->modified_timestamp,
    "modified_by_id" => $design_types->modified_by_id,
    "deleted_by_id" => $design_types->deleted_by_id,
    "deleted_at" => $design_types->deleted_at

	);

	// set response code - 200 OK
	http_response_code(200);

	// make it json format
	echo json_encode($design_types_arr);
}

else{
	// set response code - 404 Not found
    http_response_code(404);

	// tell the user the design type does not exist
	echo json_encode(array("message" => "Design Type does not exist."));
}
?>
