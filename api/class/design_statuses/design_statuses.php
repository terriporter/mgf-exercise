<?php
class Design_Status{

	// database connection and table name
	private $conn;
	private $table_name = "design_statuses";

	// object properties
	public $designs;
	public $design_categories;
	public $design_engineers;
	public $design_issues;
	public $design_senior_engineers;
	public $design_statuses;
	public $design_types;

	// constructor with $db as database connection
	public function __construct($db){
		$this->conn = $db;
	}


// read design statuses
function read(){

	// select all query
	$query = "SELECT
			*
			FROM
				" . $this->table_name;

	// prepare query statement
	$stmt = $this->conn->prepare($query);

	// execute query
	$stmt->execute();

	return $stmt;
}

function readOne(){

	// query to read single record
	$query = "SELECT
				*
			FROM
				" . $this->table_name . "
			WHERE
				id = ?
			LIMIT
				0,1";

	// prepare query statement
	$stmt = $this->conn->prepare( $query );

	// bind
	$stmt->bindParam(1, $this->id);

	// execute query
	$stmt->execute();

	// get retrieved row
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	// set values to object properties
  $this->designs = $row['designs'];
  $this->design_categories = $row['design_categories'];
	$this->design_engineers = $row['design_engineers'];
	$this->design_issues = $row['design_issues'];
	$this->design_senior_engineers = $row['design_senior_engineers'];
	$this->design_statuses = $row['design_statuses'];
  $this->design_types = $row['design_types'];
}






}

?>
