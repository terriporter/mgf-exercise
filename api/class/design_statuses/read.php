<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../../conn/database.php';
include_once 'design_statuses.php';

// instantiate database
$database = new Database();
$db = $database->getConnection();

// initialize object
$design_status= new Design_Status($db);

// query design status
$stmt = $design_status->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){

	// engineer array
	$design_status_arr=array();
	$design_status_arr["records"]=array();

	// retrieve our table contents
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		// extract row
		// this will make $row['name'] to
		// just $name only
		extract($row);
    //print_r($row);
		$design_status_item=array(
			"designs" => $designs,
      "design_categories" => $design_categories,
			"design_engineers" => $design_engineers,
      "design_issues" => $design_issues,
      "design_senior_engineers" => $design_senior_engineers,
      "design_statuses" => $design_statuses,
      "design_types" => $design_types
		);

		array_push($design_status_arr["records"], $design_status_item);
	}

	// set response code - 200 OK
	http_response_code(200);

	// show products data in json format
	echo json_encode($design_status_arr);
}

// no design statuses found will be here

else{

	// set response code - 404 Not found
	http_response_code(404);

	// tell the user no design statuses found
	echo json_encode(
		array("message" => "No Design Statuses found.")
	);
}




?>
