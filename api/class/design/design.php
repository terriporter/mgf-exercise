<?php
class Designs{

	// database connection and table name
	private $conn;
	private $table_name = "designs";

	// object properties
	public $id;
	public $customer_id;
	public $location_id;
	public $contractor_id;
	public $title;
	public $special_project;
	public $permanent_works;
  public $depth;
  public $length;
  public $width;
  public $design_type_id;

	// constructor with $db as database connection
	public function __construct($db){
		$this->conn = $db;
	}


// read designs
function read(){

	// select all query
	$query = "SELECT
			*
			FROM
				" . $this->table_name;

	// prepare query statement
	$stmt = $this->conn->prepare($query);

	// execute query
	$stmt->execute();

	return $stmt;
}

function readOne(){

	// query to read single record
	$query = "SELECT
				*
			FROM
				" . $this->table_name . "
			WHERE
				id = ?
			LIMIT
				0,1";

	// prepare query statement
	$stmt = $this->conn->prepare( $query );

	// bind id of product to be updated
	$stmt->bindParam(1, $this->id);

	// execute query
	$stmt->execute();

	// get retrieved row
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	// set values to object properties
  $this->id = $row['id'];
  $this->customer_id = $row['customer_id'];
	$this->location_id = $row['location_id'];
	$this->contractor_id = $row['contractor_id'];
	$this->title = $row['title'];
	$this->special_project = $row['special_project'];
  $this->permanent_works = $row['permanent_works'];
  $this->depth = $row['depth'];
  $this->length = $row['length'];
  $this->width = $row['width'];
  $this->design_type_id = $row['design_type_id'];
}


// update the design
function update(){

	// update query
	$query = "UPDATE
				" . $this->table_name . "
			SET
				customer_id = :customer_id,
				location_id = :location_id,
				contractor_id = :contractor_id,
				title = :title,
        special_project = :special_project,
        permanent_works = :permanent_works,
        depth = :depth,
        length = :length,
        width = :width,
        design_type_id = :design_type_id
			WHERE
				id = :id";

	// prepare query statement
	$stmt = $this->conn->prepare($query);

	// sanitize
	$this->customer_id=htmlspecialchars(strip_tags($this->customer_id));
	$this->location_id=htmlspecialchars(strip_tags($this->location_id));
	$this->contractor_id=htmlspecialchars(strip_tags($this->contractor_id));
	$this->title=htmlspecialchars(strip_tags($this->title));
  $this->special_project=htmlspecialchars(strip_tags($this->special_project));
  $this->permanent_works=htmlspecialchars(strip_tags($this->permanent_works));
  $this->depth=htmlspecialchars(strip_tags($this->depth));
  $this->length=htmlspecialchars(strip_tags($this->length));
  $this->width=htmlspecialchars(strip_tags($this->width));
  $this->design_type_id=htmlspecialchars(strip_tags($this->design_type_id));

	// bind new values
  $stmt->bindParam(':id', $this->id);
  $stmt->bindParam(':customer_id', $this->customer_id);
	$stmt->bindParam(':location_id', $this->location_id);
	$stmt->bindParam(':contractor_id', $this->contractor_id);
	$stmt->bindParam(':title', $this->title);
  $stmt->bindParam(':special_project', $this->special_project);
  $stmt->bindParam(':permanent_works', $this->permanent_works);
  $stmt->bindParam(':depth', $this->depth);
  $stmt->bindParam(':length', $this->length);
  $stmt->bindParam(':width', $this->width);
  $stmt->bindParam(':design_type_id', $this->design_type_id);

	// execute the query
	if($stmt->execute()){
		return true;
	}

	return false;
}

// delete the design
function delete(){

  $query = "SELECT COUNT(*) as total_rows, 'something' as something_else FROM " . $this->table_name . " WHERE id = ?";

	$stmt = $this->conn->prepare( $query );
  // sanitize
  $this->id=htmlspecialchars(strip_tags($this->id));

  // bind id of record to delete
  $stmt->bindParam(1, $this->id);

	$stmt->execute();
	$data = $stmt->fetch(PDO::FETCH_ASSOC);

  if ($data['total_rows'] != 1) {
    return false;
  }



	// delete query
	$query = "DELETE FROM " . $this->table_name . " WHERE id = ?";

	// prepare query
	$stmt = $this->conn->prepare($query);

	// sanitize
	$this->id=htmlspecialchars(strip_tags($this->id));

	// bind id of record to delete
	$stmt->bindParam(1, $this->id);

	// execute query
	if($stmt->execute()){
		return true;
	}

	return false;

}

// create design
function create(){

	// query to insert record
	$query = "INSERT INTO
				" . $this->table_name . "
			SET
				customer_id=:customer_id, location_id=:location_id, contractor_id=:contractor_id, title=:title, special_project=:special_project, permanent_works=:permanent_works, depth=:depth, length=:length, width=:width, design_type_id=:design_type_id";

	// prepare query
	$stmt = $this->conn->prepare($query);

	// sanitize
  $this->customer_id=htmlspecialchars(strip_tags($this->customer_id));
	$this->location_id=htmlspecialchars(strip_tags($this->location_id));
	$this->contractor_id=htmlspecialchars(strip_tags($this->contractor_id));
	$this->title=htmlspecialchars(strip_tags($this->title));
  $this->special_project=htmlspecialchars(strip_tags($this->special_project));
  $this->permanent_works=htmlspecialchars(strip_tags($this->permanent_works));
  $this->depth=htmlspecialchars(strip_tags($this->depth));
  $this->length=htmlspecialchars(strip_tags($this->length));
  $this->width=htmlspecialchars(strip_tags($this->width));
  $this->design_type_id=htmlspecialchars(strip_tags($this->design_type_id));

	// bind values
  $stmt->bindParam(':customer_id', $this->customer_id);
	$stmt->bindParam(':location_id', $this->location_id);
	$stmt->bindParam(':contractor_id', $this->contractor_id);
	$stmt->bindParam(':title', $this->title);
  $stmt->bindParam(':special_project', $this->special_project);
  $stmt->bindParam(':permanent_works', $this->permanent_works);
  $stmt->bindParam(':depth', $this->depth);
  $stmt->bindParam(':length', $this->length);
  $stmt->bindParam(':width', $this->width);
  $stmt->bindParam(':design_type_id', $this->design_type_id);

	// execute query
	if($stmt->execute()){
		return true;
	}

	return false;

}


}

?>
