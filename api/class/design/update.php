<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../../../conn/database.php';
include_once 'design.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare design object
$designs = new Designs($db);

// get id of design to be edited
$data = json_decode(file_get_contents("php://input"));

// set ID property of design to be edited
$designs->id = $data->id;

// set design property values
$designs->customer_id = $data->customer_id;
$designs->location_id = $data->location_id;
$designs->contractor_id = $data->contractor_id;
$designs->title = $data->title;
$designs->special_project = $data->special_project;
$designs->permanent_works = $data->permanent_works;
$designs->depth = $data->depth;
$designs->length = $data->length;
$designs->width = $data->width;
$designs->design_type_id = $data->design_type_id;

// update the design
if($designs->update()){

	// set response code - 200 ok
	http_response_code(200);

	// tell the user
	echo json_encode(array("message" => "Design was updated."));
}

// if unable to update the design, tell the user
else{

	// set response code - 503 service unavailable
	http_response_code(503);

	// tell the user
	echo json_encode(array("message" => "Unable to update design."));
}
?>
