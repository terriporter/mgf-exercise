<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../../../conn/database.php';
include_once 'design.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare design object
$designs = new Designs($db);

// set ID property of record to read
$designs->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of design to be edited
$designs->readOne();

if($designs->id!=null){
	// create array
	$designs_arr = array(
		"id" =>  $designs->id,
    "customer_id" => $designs->customer_id,
		"location_id" => $designs->location_id,
		"contractor_id" => $designs->contractor_id,
    "title" => $designs->title,
    "special_project" => $designs->special_project,
    "permanent_works" => $designs->permanent_works,
    "depth" => $designs->depth,
    "length" => $designs->length,
    "width" => $designs->width,
    "design_type_id" => $designs->design_type_id

	);

	// set response code - 200 OK
	http_response_code(200);

	// make it json format
	echo json_encode($designs_arr);
}

else{
	// set response code - 404 Not found
    http_response_code(404);

	// tell the user design does not exist
	echo json_encode(array("message" => "Design does not exist."));
}
?>
