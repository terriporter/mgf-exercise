<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../../conn/database.php';
include_once 'design.php';

// instantiate database
$database = new Database();
$db = $database->getConnection();

// initialize object
$designs = new Designs($db);

// query design
$stmt = $designs->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){

	// design array
	$designs_arr=array();
	$designs_arr["records"]=array();

	// retrieve our table contents
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		// extract row
		// this will make $row['name'] to
		// just $name only
		extract($row);
    //print_r($row);
		$design_item=array(
			"id" => $id,
      "customer_id" => $customer_id,
			"location_id" => $location_id,
      "contractor_id" => $contractor_id,
      "title" => $title,
      "special_project" => $special_project,
      "permanent_works" => $permanent_works,
      "depth" => $depth,
      "length" => $length,
      "width" => $width,
      "design_type_id" => $design_type_id
		);

		array_push($designs_arr["records"], $design_item);
	}

	// set response code - 200 OK
	http_response_code(200);

	// show design data in json format
	echo json_encode($designs_arr);
}

// no designs found will be here

else{

	// set response code - 404 Not found
	http_response_code(404);

	// tell the user no designs found
	echo json_encode(
		array("message" => "No Designs found.")
	);
}




?>
