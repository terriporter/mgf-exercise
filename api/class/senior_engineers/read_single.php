<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../../../conn/database.php';
include_once 'senior_engineers.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare senior engineer object
$senior_engineer = new Senior_Engineer($db);

// set ID property of record to read
$senior_engineer->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of senior engineer to be edited
$senior_engineer->readOne();

if($senior_engineer->userid!=null){
	// create array
	$senior_engineer_arr = array(
		"id" =>  $senior_engineer->id,
    "userid" => $senior_engineer->userid,
		"name" => $senior_engineer->name,
		"initial" => $senior_engineer->initial,
    "grade" => $senior_engineer->grade,
    "title" => $senior_engineer->title,
    "qualifications" => $senior_engineer->qualifications,
    "eductaion" => $senior_engineer->eductaion,
    "synopsis" => $senior_engineer->synopsis,
    "memberships" => $senior_engineer->memberships,
    "experience" => $senior_engineer->experience

	);

	// set response code - 200 OK
	http_response_code(200);

	// make it json format
	echo json_encode($senior_engineer_arr);
}

else{
	// set response code - 404 Not found
    http_response_code(404);

	// tell the user senior engineer does not exist
	echo json_encode(array("message" => "Senior Engineer does not exist."));
}
?>
