<?php
class Senior_Engineer{

	// database connection and table name
	private $conn;
	private $table_name = "design_senior_engineers";

	// object properties
	public $id;
	public $userid;
	public $name;
	public $initial;
	public $grade;
	public $title;
	public $qualifications;
  public $eductaion;
  public $synopsis;
  public $memberships;
  public $experience;

	// constructor with $db as database connection
	public function __construct($db){
		$this->conn = $db;
	}


// read senior engineers
function read(){

	// select all query
	$query = "SELECT
			*
			FROM
				" . $this->table_name;

	// prepare query statement
	$stmt = $this->conn->prepare($query);

	// execute query
	$stmt->execute();

	return $stmt;
}

function readOne(){

	// query to read single record
	$query = "SELECT
				*
			FROM
				" . $this->table_name . "
			WHERE
				id = ?
			LIMIT
				0,1";

	// prepare query statement
	$stmt = $this->conn->prepare( $query );

	// bind id of senior engineer to be updated
	$stmt->bindParam(1, $this->id);

	// execute query
	$stmt->execute();

	// get retrieved row
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	// set values to object properties
  $this->userid = $row['userid'];
  $this->name = $row['name'];
	$this->inital = $row['initial'];
	$this->grade = $row['grade'];
	$this->title = $row['title'];
	$this->qualifications = $row['qualifications'];
  $this->eductaion = $row['eductaion'];
  $this->synopsis = $row['synopsis'];
  $this->memberships = $row['memberships'];
  $this->experience = $row['experience'];
}






}

?>
