<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../../conn/database.php';
include_once 'senior_engineers.php';

// instantiate database
$database = new Database();
$db = $database->getConnection();

// initialize object
$senior_engineer= new Senior_Engineer($db);

// query senior engineer
$stmt = $senior_engineer->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){

	// senior engineer array
	$senior_engineers_arr=array();
	$senior_engineers_arr["records"]=array();

	// retrieve our table contents
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		// extract row
		// this will make $row['name'] to
		// just $name only
		extract($row);
    //print_r($row);
		$senior_engineer_item=array(
			"id" => $id,
      "userid" => $userid,
			"name" => $name,
      "initial" => $initial,
      "grade" => $grade,
      "title" => $title,
      "qualifications" => $qualifications,
      "eductaion" => $eductaion,
      "synopsis" => $synopsis,
      "memberships" => $memberships,
      "experience" => $experience
		);

		array_push($senior_engineers_arr["records"], $senior_engineer_item);
	}

	// set response code - 200 OK
	http_response_code(200);

	// show products data in json format
	echo json_encode($senior_engineers_arr);
}

// no senior engineers found will be here

else{

	// set response code - 404 Not found
	http_response_code(404);

	// tell the user no senior engineers found
	echo json_encode(
		array("message" => "No Senior Engineers found.")
	);
}




?>
