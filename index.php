<!DOCTYPE html>
<html>
<head>
<title>MGF Exercise</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
</head>
<body>
    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">MGF Exercise</a>
        </div>
    </div>
    <div id="main" class="container">

        <h3>Design Table</h3>
        <button onclick = "createclick()">Create a design</button>
        <table class="table" id="designstable">
          <tr>
            <th>Id:</th><th>Customer Id:</th><th>Location Id:</th><th>Contractor Id:</th><th>Title:</th><th>Special Project:</th>
            <th>Permanent Works:</th><th>Dimensions</th><th>Design Type Id:</th><th>Actions:</th>
          </tr>
        </table>


    </div>
    <script type="text/javascript">

    // Create a request variable and assign a new XMLHttpRequest object to it.
      var request = new XMLHttpRequest()

    // Open a new connection, using the GET request on the URL endpoint
    request.open('GET', 'http://192.168.0.33/webserver/MGF%20exercise/api/class/design/read.php', true)

    request.onload = function () {
      // Begin accessing JSON data here
  var data = JSON.parse(this.response)
 var tablehtml = "";
  data.records.forEach(designs => {
    //console.log(designs.title)

    tablehtml += `<tr>
        <td>${designs.id}</td>
        <td>${designs.customer_id}</td>
        <td>${designs.location_id}</td>
        <td>${designs.contractor_id}</td>
        <td>${designs.title}</td>
        <td>${designs.special_project}</td>
        <td>${designs.permanent_works}</td>
        <td>D: ${designs.depth} <br/> L: ${designs.length} <br/> W: ${designs.width} </td>
        <td>${designs.design_type_id}</td>
        <td><button onclick = "issuesclick(${designs.id})">View Design Issues</button></td>
        <td><button onclick = "updateclick(${designs.id})">Update</button></td>
        <td><button onclick = "deleteclick(${designs.id})">Delete</button></td>
      </tr>
    `
  })
//tablehtml += `<tr><td>something</td></tr>`
document.getElementById("designstable").innerHTML +=  tablehtml;
    }

    // Send request
    request.send()

//Function to catch the delete button
function deleteclick(delid) {

  // Create a request variable and assign a new XMLHttpRequest object to it.
    var delrequest = new XMLHttpRequest()
    delrequest.open('POST', 'http://192.168.0.33/webserver/MGF%20exercise/api/class/design/delete.php', true)
    var json = `{"id": "${delid}"}`
    delrequest.send(json);

    delrequest.onreadystatechange = function(){
      if (this.readyState == 4 && this.status == 200) {
        alert('Deleted')
        reload()
      } else if(this.readyState == 4 && this.status != 200) {
        alert('An error occured whilst deleteing the design')
      }
    }
}

//Function to send to the create form page
function createclick() {
  window.location.href = 'createForm.php'
}

//Function to send to update form, sends id so the details can be retrieved
function updateclick(upid) {

  window.location.href = `updateForm.php?id=${upid}`
}

//Function to send to design issues page, sends id to retrieve details
function issuesclick(designid) {

  window.location.href = `designIssues.php?designid=${designid}`
}



    </script>
</body>
</html>
