<!DOCTYPE html>
<html>
<head>
<title>MGF Exercise</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
</head>
<body>
    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">Update the Design</a>
        </div>
    </div>
    <div id="main" class="container">
      <form id="createdesign" name="designForm" method="POST" action="">
          <input type="hidden" id ="id" name="id">
          Customer Id: <input type="text" id ="customer_id" name="customer_id">
          <br>
          Location Id: <input type="text" name="location_id">
            <br>
          Contractor Id: <input type="text" name="contractor_id">
          <br>
          Title: <input type="text" name="title">
          <br>
          Special Project: <input type="text" name="special_project">
          <br>
          Permanent Works: <input type="text" name="permanent_works">
          <br>
          Depth: <input type="text" name="depth">
          <br>
          Length: <input type="text" name="length">
          <br>
          Width: <input type="text" name="width">
          <br>
          Design Type Id: <input type="text" name="design_type_id">
          <br>
          <input type="submit" value="Submit">
      </form>
    </div>

<script type="text/javascript">

//Get the previous values from the database
function getdesign(id) {

  // Create a request variable and assign a new XMLHttpRequest object to it.
    var request = new XMLHttpRequest()

  // Open a new connection, using the GET request on the URL endpoint
  request.open('GET', 'http://192.168.0.33/webserver/MGF%20exercise/api/class/design/read_single.php' +'?id=' + id, true)

  request.onload = function () {
    // Begin accessing JSON data here
var olddata = JSON.parse(this.response)

//Update the form with the old database
document.getElementsByName('id')[0].value= olddata.id;
document.getElementsByName('customer_id')[0].value= olddata.customer_id;
document.getElementsByName('location_id')[0].value= olddata.location_id;
document.getElementsByName('contractor_id')[0].value= olddata.contractor_id;
document.getElementsByName('title')[0].value= olddata.title;
document.getElementsByName('special_project')[0].value= olddata.special_project;
document.getElementsByName('permanent_works')[0].value= olddata.permanent_works;
document.getElementsByName('depth')[0].value= olddata.depth;
document.getElementsByName('length')[0].value= olddata.length;
document.getElementsByName('width')[0].value= olddata.width;
document.getElementsByName('design_type_id')[0].value= olddata.design_type_id;

}
// Send request
request.send()
}

function toJSONString( form ) {
  var obj = {};
  var elements = form.querySelectorAll( "input, select, textarea" );
  for( var i = 0; i < elements.length; ++i ) {
    var element = elements[i];
    var name = element.name;
    var value = element.value;

    if( name ) {
      obj[ name ] = value;
    }
  }

  return JSON.stringify( obj );
}

function updatedesign(json) {

  // Create a request variable and assign a new XMLHttpRequest object to it.
    var request = new XMLHttpRequest()
    request.open('POST', 'http://192.168.0.33/webserver/MGF%20exercise/api/class/design/update.php', true)

    request.send(json);

    request.onreadystatechange = function(){
      if (this.readyState == 4 && this.status == 200) {
        alert('Updated')
      } else if(this.readyState == 4 && this.status != 200) {
        alert('An error occured whilst updating the design')
      }
    }
  }



	document.addEventListener( "DOMContentLoaded", function() {
		var form = document.getElementById( "createdesign" );
		//var output = document.getElementById( "output" );
		form.addEventListener( "submit", function( e ) {
			e.preventDefault();
			var json = toJSONString( this );
			updatedesign(json);

		}, false);


})


//Read the get prams
var queryDict = {}
location.search.substr(1).split("&").forEach(function(item) {queryDict[item.split("=")[0]] = item.split("=")[1]})
getdesign(queryDict['id'])

</script>


</body>
</html>
