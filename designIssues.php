<!DOCTYPE html>
<html>
<head>
<title>MGF Exercise</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
</head>
<body>
    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">Design Issues</a>
        </div>
    </div>
    <div id="main" class="container">

        <h3>Design Issues Table</h3>
        <table class="table" id="designstable">
          <tr>
            <th>Id:</th><th>Design Id:</th><th>Category Id:</th><th>Description:</th><th>Date In:</th><th>Date out:</th>
            <th>Designer Id:</th><th>Checker Id</th><th>Status Id:</th><th>Drawing Req:</th>
          </tr>
        </table>


    </div>
    <script type="text/javascript">

    // Create a request variable and assign a new XMLHttpRequest object to it.
      var request = new XMLHttpRequest()

    // Open a new connection, using the GET request on the URL endpoint
    var queryDict = {}
    location.search.substr(1).split("&").forEach(function(item) {queryDict[item.split("=")[0]] = item.split("=")[1]})
    $design_id = queryDict['designid']
    request.open('GET', 'http://192.168.0.33/webserver/MGF%20exercise/api/class/design_issues/read_by_designID.php?id=' + $design_id, true)

    request.onload = function () {
      // Begin accessing JSON data here
  var data = JSON.parse(this.response)
 var tablehtml = "";
  data.records.forEach(designs => {
    //console.log(designs.title)

    tablehtml += `<tr>
        <td>${designs.id}</td>
        <td>${designs.design_id}</td>
        <td>${designs.category_id}</td>
        <td>${designs.description}</td>
        <td>${designs.date_in}</td>
        <td>${designs.date_out}</td>
        <td>${designs.designer_id}</td>
        <td>${designs.checker_id}</td>
        <td>${designs.status_id}</td>
        <td>${designs.drawing_req}</td>
      </tr>
    `
  })
//tablehtml += `<tr><td>something</td></tr>`
document.getElementById("designstable").innerHTML +=  tablehtml;
    }

    // Send request
    request.send()

//Function to catch the delete button
function deleteclick(delid) {

  // Create a request variable and assign a new XMLHttpRequest object to it.
    var delrequest = new XMLHttpRequest()
    delrequest.open('POST', 'http://192.168.0.33/webserver/MGF%20exercise/api/class/design/delete.php', true)
    var json = `{"id": "${delid}"}`
    delrequest.send(json);

    delrequest.onreadystatechange = function(){
      if (this.readyState == 4 && this.status == 200) {
        alert('Deleted')
        reload()
      } else if(this.readyState == 4 && this.status != 200) {
        alert('An error occured whilst deleteing the design')
      }
    }
}

function createclick() {
  window.location.href = 'createForm.php'
}

function updateclick(upid) {

  window.location.href = `updateForm.php?id=${upid}`
}





    </script>
</body>
</html>
